﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_API.Models
{
    public class Employment
    {
        public int EmploymentId { get; set; }
        public String Name { get; set; }
        public String Country { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }
        public bool ActiveEmployment { get; set; }
    }
}
