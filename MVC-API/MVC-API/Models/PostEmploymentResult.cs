﻿using Microsoft.AspNetCore.Cors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_API.Models
{
    
    public class PostEmploymentResult
    {
        public bool Remove { get; set; }
        public PostEmploymentResult(bool remove) {
            Remove = remove;
        }
        
    }
}
