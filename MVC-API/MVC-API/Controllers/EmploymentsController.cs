﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVC_API.Data;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmploymentsController : ControllerBase
    {
     
      
        [HttpPost]
        public ActionResult<PostEmploymentResult> PostEmployment(Employment employment)
        {
   
            bool is_below_30 = employment.Age < 30;

            return new PostEmploymentResult(is_below_30); 
        }

 
      
    }
}
