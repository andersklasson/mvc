﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVC_API.Models;

namespace MVC_API.Data
{
    public class MVC_APIContext : DbContext
    {
        public MVC_APIContext (DbContextOptions<MVC_APIContext> options)
            : base(options)
        {
        }

        public DbSet<MVC_API.Models.Employment> Employment { get; set; }
    }
}
